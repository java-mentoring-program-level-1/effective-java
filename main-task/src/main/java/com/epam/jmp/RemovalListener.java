package com.epam.jmp;

public interface RemovalListener<K, V> {
  void onRemoval(Node<CacheElement<K, V>> node);
}
