package com.epam.jmp;

import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class CacheImpl<K, V> implements Cache<K, V> {
  private final int size;
  private final DoublyLinkedList<CacheElement<K, V>> linkedList;
  private final Map<K, Node<CacheElement<K, V>>> nodeMap;
  private final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
  private final long expiringTime;
  private final RemovalListener<K, V> removalListener;
  private final StatisticReporter statisticReporter;
  private AtomicInteger evictionCounter = new AtomicInteger();
  private Queue<Long> timesOfSettingValue;
  private boolean cacheAlive = true;

  public CacheImpl(Builder builder) {
    this.size = builder.size;
    this.nodeMap = new ConcurrentHashMap<>(size);
    this.timesOfSettingValue = new ConcurrentLinkedDeque<>();
    this.linkedList = new DoublyLinkedList<>();
    this.expiringTime = builder.expiringTime;
    this.removalListener = builder.removalListener;
    this.statisticReporter = builder.statisticReporter;
    initialize();
  }

  private void initialize() {
    new CleanerThread().start();
  }

  @Override
  public void quitCache() {
    this.cacheAlive = false;
    if(statisticReporter != null) {
      OptionalDouble average = timesOfSettingValue
        .stream()
        .mapToDouble(time -> time)
        .average();
      String reportText = "Average time for putting a value into this cache: " + (average.isPresent() ? average.getAsDouble(): 0);
      statisticReporter.report(reportText);
    }
  }

  public boolean isCacheAlive() {
    return cacheAlive;
  }

  @Override
  public boolean put(K key, V value) {
    this.lock.writeLock().lock();
    long start = System.currentTimeMillis();
    try {
      CacheElement<K, V> element = new CacheElement<>(key, value);
      Node<CacheElement<K, V>> newNode;
      if(this.nodeMap.containsKey(key)) {
        Node<CacheElement<K, V>> node = this.nodeMap.get(key);
        node.getElement().updateAccessTime();
        newNode = linkedList.updateAndMoveToFront(node, element);
      } else {
        if(this.size() >= this.size) {
          return false;
        }
        newNode = this.linkedList.add(element);
      }
      if(newNode.isEmpty()) {
        return false;
      }
      this.nodeMap.put(key, newNode);
      return true;
    } finally {
      long end = System.currentTimeMillis();
      long time = end - start;
      timesOfSettingValue.add(time);
      this.lock.writeLock().unlock();
    }
  }


  @Override
  public Optional<V> get(K key) {
    this.lock.readLock().lock();
    try {
      Node<CacheElement<K, V>> node = this.nodeMap.get(key);
      if(node != null && !linkedList.isEmpty()) {
        node.getElement().updateAccessTime();
        nodeMap.put(key, this.linkedList.moveToFront(node));
        return Optional.of(node.getElement().getValue());
      }
      return Optional.empty();
    } finally {
      this.lock.readLock().unlock();
    }
  }

  @Override
  public int size() {
    this.lock.readLock().lock();
    try {
      return linkedList.size();
    } finally {
      this.lock.readLock().unlock();
    }
  }

  @Override
  public boolean isEmpty() {
    return size() == 0;
  }

  @Override
  public void clear() {
    this.lock.writeLock().lock();
    try {
      nodeMap.clear();
      linkedList.clear();
    } finally {
      this.lock.writeLock().unlock();
    }
  }

  public static class Builder {
    private int size = 100000;
    private long expiringTime;
    private RemovalListener removalListener;
    private StatisticReporter statisticReporter;

    public Builder() {}

    public Builder maximumSize(int maximumSize) {
      this.size = maximumSize;
      return this;
    }

    public Builder expiringTime(long expiringTimeInMilliSecond) {
      this.expiringTime = expiringTimeInMilliSecond;
      return this;
    }

    public <K, V> Builder removalListener(RemovalListener<K, V> removalListener) {
      this.removalListener = removalListener;
      return this;
    }

    public Builder statisticReporter(StatisticReporter reporter) {
      this.statisticReporter = reporter;
      return this;
    }

    public <K, V> CacheImpl<K, V> build() {
      return new CacheImpl<K, V>(this);
    }
  }

  class CleanerThread extends Thread {
    @Override
    public void run() {
      while (cacheAlive) {
        evictElements();
        try {
          Thread.sleep(expiringTime / 2);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  private void evictElements() {
    this.lock.writeLock().lock();
    try {
      Node<CacheElement<K, V>> temp = linkedList.getTail();
      while (isEvictable(temp)) {
        Node<CacheElement<K, V>> removedNode = linkedList.removeTail();
        if(removedNode == null) {
          return;
        }
        nodeMap.remove(removedNode.getElement().getKey());

        if(removalListener != null) {
          removalListener.onRemoval(removedNode);
        }

        evictionCounter.incrementAndGet();
        temp = linkedList.getTail();
      }
    } finally {
      this.lock.writeLock().unlock();
    }

  }

  @Override
  public int getEvictionCounter() {
    return evictionCounter.get();
  }

  private boolean isEvictable(Node<CacheElement<K, V>> node) {
    if(node != null && !node.isEmpty()) {
      long lastAccessTime = node.getElement().getAccessTime();
      return (System.currentTimeMillis() - lastAccessTime) >= expiringTime;
    }
    return false;
  }
}