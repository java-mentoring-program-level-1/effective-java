package com.epam.jmp;

import java.util.concurrent.atomic.AtomicInteger;

public class Launcher {
  public static void main(String[] args) {
    CacheImpl.Builder builder = new CacheImpl.Builder();
    Cache<Long, String> cache = builder.expiringTime(5000)
      .maximumSize(5)
      .removalListener((RemovalListener<Long, String>) node -> {
        System.out.println("Removed cache element: \n" +
          "Key: " + node.getElement().getKey() + "\n" +
          "Value: " + node.getElement().getValue() + "\n" +
          "Time accessed: " + node.getElement().getAccessTime());})
      .statisticReporter(System.out::println).build();

    cache.put(1L, "Hello");
    sleep(1000);
    cache.put(2L, "Hi");
    sleep(1000);
    cache.put(3L, "Hola");
    sleep(1000);
    cache.put(4L, "Salom");
    sleep(2000);
    cache.put(5L, "Ni hao1");
    cache.put(6L, "Ni hao2");

    System.out.println(cache.get(4L));

    sleep(5000);
    System.out.println("Overall eviction count: " + cache.getEvictionCounter());
    cache.quitCache();

  }

  private static void sleep(long time) {
    try {
      Thread.sleep(time);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
