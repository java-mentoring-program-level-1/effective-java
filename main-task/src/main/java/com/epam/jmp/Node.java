package com.epam.jmp;

public class Node<V> {
  private Node<V> prev;
  private Node<V> next;
  private final V value;

  public Node(V value) {
    this.value = value;
  }

  public Node(Node<V> next, Node<V> prev, V value) {
    this.next = next;
    this.prev = prev;
    this.value = value;
  }

  public boolean hasElement() {
    return true;
  }

  public boolean isEmpty() {
    return false;
  }

  public V getElement() {
    return value;
  }

  public void detach() {
    if(this.prev != null) {
      this.prev.setNext(this.getNext());
    }

    if(this.next != null) {
      this.next.setPrev(this.getPrev());
    }
  }

  public Node<V> getPrev() {
    return prev;
  }

  public Node<V> setPrev(Node<V> prev) {
    this.prev = prev;
    return this;
  }

  public Node<V> getNext() {
    return next;
  }

  public Node<V> setNext(Node<V> next) {
    this.next = next;
    return this;
  }

  public Node<V> search(V value) {
    return this.getElement() == value ? this : this.getNext().search(value);
  }

  @Override
  public String toString() {
    return "Node{" +
      "value=" + value +
      '}';
  }
}
