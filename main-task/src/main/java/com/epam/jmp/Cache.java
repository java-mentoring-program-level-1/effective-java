package com.epam.jmp;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public interface Cache <K, V> {
  boolean put(K key, V value);
  Optional<V> get(K key);
  int size();
  void quitCache();
  int getEvictionCounter();
  boolean isEmpty();
  void clear();
}
