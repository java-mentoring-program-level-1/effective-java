package com.epam.jmp;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class DoublyLinkedList<V> {
  private Node<V> head;
  private Node<V> tail;
  private AtomicInteger size;
  private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();

  public DoublyLinkedList() {
    clear();
  }

  public void clear() {
    this.lock.writeLock().lock();
    try {
      head = null;
      tail = null;
      size = new AtomicInteger(0);
    } finally {
      this.lock.writeLock().unlock();
    }
  }

  public int size() {
    this.lock.readLock().lock();
    try {
      return size.get();
    } finally {
      this.lock.readLock().unlock();
    }
  }

  public boolean isEmpty() {
    this.lock.readLock().lock();
    try {
      return head.isEmpty();
    } finally {
      this.lock.readLock().unlock();
    }
  }

  public boolean contains(V value) {
    this.lock.readLock().lock();
    try {
      return search(value).hasElement();
    } finally {
      this.lock.readLock().unlock();
    }
  }

  public Node<V> search(V value) {
    this.lock.readLock().lock();
    try {
      return head.search(value);
    } finally {
      this.lock.readLock().unlock();
    }
  }

  public Node<V> add(V value) {
    this.lock.writeLock().lock();
    try {
      Node<V> newNode = new Node<V>(head, null , value);
      if(head != null) {
        head.setPrev(newNode);
      }
      head = newNode;
      if(tail == null || tail.isEmpty()) {
        tail = head;
      }
      size.incrementAndGet();
      return head;
    } finally {
      this.lock.writeLock().unlock();
    }
  }

  public Node<V> remove(V value) {
    this.lock.writeLock().lock();
    try {
      Node<V> linkedListNode = head.search(value);
      if (!linkedListNode.isEmpty()) {
        if (linkedListNode == tail) {
          tail = tail.getPrev();
        }
        if (linkedListNode == head) {
          head = head.getNext();
        }
        linkedListNode.detach();
        size.decrementAndGet();
      }
      return linkedListNode;
    } finally {
      this.lock.writeLock().unlock();
    }
  }

  public Node<V> moveToFront(Node<V> node) {
    return node.isEmpty() ? null : updateAndMoveToFront(node, node.getElement());
  }

  public Node<V> updateAndMoveToFront(Node<V> node, V newValue) {
    this.lock.writeLock().lock();
    try {
      detach(node);
      add(newValue);
      return head;
    } finally {
      this.lock.writeLock().unlock();
    }
  }

  private void detach(Node<V> node) {
    if(node != tail) {
      node.detach();
      if(node == head) {
        head = head.getNext();
      }
      size.decrementAndGet();
    } else {
      removeTail();
    }
  }

  public Node<V> removeTail() {
    this.lock.writeLock().lock();
    try {
      Node<V> oldTail = tail;
      if (oldTail == head) {
        tail = head = null;
      } else {
        tail = tail.getPrev();
        oldTail.detach();
      }
      if (!oldTail.isEmpty()) {
        size.decrementAndGet();
      }
      return oldTail;
    } finally {
      this.lock.writeLock().unlock();
    }
  }

  public Node<V> getTail() {
    this.lock.readLock().lock();
    try {
      return tail;
    } finally {
      this.lock.readLock().unlock();
    }
  }

  public void print() {
    Node<V> temp = head;
    while (temp != null) {
      System.out.println(temp.getElement());
      temp = temp.getNext();
    }
  }
}
