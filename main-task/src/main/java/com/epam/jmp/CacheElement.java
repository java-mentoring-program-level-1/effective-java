package com.epam.jmp;

import java.time.LocalDateTime;

public class CacheElement<K, V> {
  private final K key;
  private final V value;
  private long accessTime;

  public CacheElement(K key, V value) {
    this.key = key;
    this.value = value;
    this.accessTime = System.currentTimeMillis();
  }

  public K getKey() {
    return key;
  }

  public V getValue() {
    return value;
  }

  public void updateAccessTime() {
    accessTime = System.currentTimeMillis();
  }

  public long getAccessTime() {
    return accessTime;
  }

  @Override
  public String toString() {
    return "CacheElement{" +
      "key=" + key +
      ", value=" + value +
      ", accessTime=" + accessTime +
      '}';
  }
}
